﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using static OpenSlide.Cs.WebApp.Models.DefaultOptions;

namespace OpenSlide.Cs.WebApp.Models
{
	public class Slide
	{
		public string FileName { get; set; }
		public string SlideUrl { get; set; }
		public string SlideDziUrl { get; set; }
		public double SlideMpp { get; set; }

		public IDictionary<string, string> SlideProperties { get; set; }
		public IDictionary<string, AssociatedImage> AssociatedImages { get; set; }

		public DeepZoomGenerator DeepZoomGenerator { get; set; }
		public ImageCodecInfo FormatEncoder { get; set; }
		public EncoderParameters QualityParameter { get; set; }

		public Slide(){ }

		public Slide(string pathToSlide)
		{
			var osr = new OpenSlide(pathToSlide);

			try
			{
				double.TryParse(osr.Properties[osr.PROPERTY_NAME_MPP_X], out double mppX);
				double.TryParse(osr.Properties[osr.PROPERTY_NAME_MPP_Y], out double mppY);
				SlideMpp = (mppX + mppY) / 2;
			}
			catch(Exception)
			{
				SlideMpp = 0;
			}

			var fileName = Path.GetFileName(pathToSlide);
			FileName = fileName;
			SlideUrl = UrlFormatter.UrlFor(fileName);
			SlideDziUrl = SlideUrl + ".dzi";

			SlideProperties = osr.Properties;
			AssociatedImages = osr.AssociatedImages;
			
			DeepZoomGenerator = new DeepZoomGenerator(osr, DEEPZOOM_TILE_SIZE, DEEPZOOM_OVERLAP);
			QualityParameter = GetQualityEncoderParameter();
			FormatEncoder = GetEncoder(DEEPZOOM_FORMAT == "png" ? ImageFormat.Png : ImageFormat.Jpeg);
		}
		
	}
}
