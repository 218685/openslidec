﻿namespace OpenSlide.Cs
{
	public class LevelSize
	{
		public long Width { get; set; }

		public long Height { get; set; }

		public LevelSize(long width, long height)
		{
			Width = width;
			Height = height;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;

			var ls = (LevelSize) obj;
			return Width.Equals(ls.Width) && Height.Equals(ls.Height);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return "w:" + Width + " h:" + Height;
		}
	}
}