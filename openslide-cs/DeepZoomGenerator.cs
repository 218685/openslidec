﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using OpenSlide.Cs.Helpers;

namespace OpenSlide.Cs
{
	public class DeepZoomGenerator
	{
		#region Properties
		public int MaxDeepZoomLevel { get; set; }
		public LevelSize[] TileDimensions { get; set; }
		public List<LevelSize> DeepZoomLevelDimensions { get; set; }
		public int DeepZoomTilesCount { get; set; }
		public int TileSize { get; set; }
		public int DeepZoomOverlap { get; set; }
		public bool LimitBounds { get; set; }
		public OpenSlide Osr { get; set; }
		public LevelSize Level0Offset { get; set; }
		internal List<LevelSize> LevelDimensions { get; set; }
		internal LevelSize Level0Dimensions { get; set; }
		public int[] Level0DeepZoomDownsamples { get; set; }
		public int[] BestSlideLevelForDeepZoomLevel { get; set; }
		public double[] DeepZoomLevelDownsamples { get; set; }
		public string DeepZoomImageTemplate { get; } =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Image xmlns=\"http://schemas.microsoft.com/deepzoom/2008\" Format=\"@Format\"  Overlap=\"@Overlap\"  TileSize=\"@TileSize\">" +
			"<Size Height=\"@Height\" Width=\"@Width\"/></Image>";
		public Color BackgroundColor { get; set; }

		#endregion
		/* """Create a DeepZoomGenerator wrapping an OpenSlide object.
        osr:          a slide object.
        tile_size:    the width and height of a single tile.  For best viewer performance, tile_size+2*overlap 
					  should be a power of two.
        overlap:      the number of extra pixels to add to each interior edge of a tile.
        limit_bounds: True to render only the non-empty slide region.""" */
		public DeepZoomGenerator(OpenSlide osr, int tileSize = 254, int overlap = 1, bool limitBounds = true)
		{
			Osr = osr;
			TileSize = tileSize;
			DeepZoomOverlap = overlap;
			LimitBounds = limitBounds;

			//Precompute dimensions
			if (LimitBounds)
				PrecomputeLimitedDimensions();
			else
				PrecomputeDimenstions();
			Level0Dimensions = LevelDimensions[0];
			GetDeepZoomLevels();
			GetTileDimensions();
			GetMaxDeepZoomLevel();
			// Total downsamples for each Deep Zoom level
			GetLevel0DeepZoomDownsamples();
			// Preferred slide levels for each Deep Zoom level
			GetBestSlideLevelsForDeepZoomLevels();
			// Piecewise downsamples
			GetDeepZoomDownsamples();
			GetBackgroundColor();
			GetTilesCount();
		}

		private void PrecomputeLimitedDimensions()
		{
			DebugTrace.Leave("start limit bounds calculations");
			// Level 0 coordinate offset
			long.TryParse(Osr.Properties[Osr.PROPERTY_NAME_BOUNDS_X], out long width);
			long.TryParse(Osr.Properties[Osr.PROPERTY_NAME_BOUNDS_Y], out long height);
			Level0Offset = new LevelSize(width, height);
			DebugTrace.Test("level 0 offset: " + Level0Offset);

			//Slide level dimensions scale factor in each axis
			float.TryParse(Osr.Properties[Osr.PROPERTY_NAME_BOUNDS_WIDTH], out float scaledWidth);
			DebugTrace.Test("bounds width to long: " + scaledWidth);
			float.TryParse(Osr.Properties[Osr.PROPERTY_NAME_BOUNDS_HEIGHT], out float scaledHeight);

			var level0Limits = Osr.LevelDimensions[0];
			DebugTrace.Test("level 0 limits : " + level0Limits);
			DebugTrace.Test("scaled width: " + scaledWidth / level0Limits.Width);
			scaledWidth = scaledWidth / level0Limits.Width;
			scaledHeight = scaledHeight / level0Limits.Height;
			DebugTrace.Test("scaled height: " + scaledWidth);
			var sizeScale = new SizeF(scaledWidth, scaledHeight);
			DebugTrace.Test("size scale: " + sizeScale);

			// Dimensions of active area
			LevelDimensions = new List<LevelSize>();
			foreach (var levelSize in Osr.LevelDimensions)
			{
				var newScaledWidth = (long)Math.Ceiling((double)levelSize.Width * sizeScale.Width);
				var newScaledHeight = (long)Math.Ceiling((double)levelSize.Height * sizeScale.Height);
				var scaledLevelSize = new LevelSize(newScaledWidth, newScaledHeight);
				LevelDimensions.Add(scaledLevelSize);
			}
			DebugTrace.Leave("end limit bounds calculations");
		}

		private void PrecomputeDimenstions()
		{
			DebugTrace.Leave("start calculations");
			LevelDimensions = Osr.LevelDimensions;
			Level0Offset = new LevelSize(0, 0);
			DebugTrace.Test("level 0 offset: " + Level0Offset);
		}

		private void GetDeepZoomLevels()
		{
			var deepZoomSize = Level0Dimensions;
			DeepZoomLevelDimensions = new List<LevelSize> { deepZoomSize };
			DebugTrace.Test("new deep zoom size: " + deepZoomSize);

			while (deepZoomSize.Width > 1 || deepZoomSize.Height > 1)
			{
				var width = (long)Math.Max(1, Math.Ceiling(deepZoomSize.Width / 2.0));
				var height = (long)Math.Max(1, Math.Ceiling(deepZoomSize.Height / 2.0));

				deepZoomSize = new LevelSize(width, height);
				DebugTrace.Test("new deep zoom size: " + deepZoomSize);
				DeepZoomLevelDimensions.Add(deepZoomSize);
			}
			DeepZoomLevelDimensions.Reverse();
		}

		private void GetTileDimensions()
		{
			TileDimensions = DeepZoomLevelDimensions.Select(
			x => new LevelSize((long)Math.Ceiling(x.Width / (double)TileSize),
				(long)Math.Ceiling(x.Height / (double)TileSize))).ToArray();
			foreach (var tileDim in TileDimensions)
				DebugTrace.Test("tileDim: " + tileDim);
		}

		private void GetMaxDeepZoomLevel()
		{
			MaxDeepZoomLevel = DeepZoomLevelDimensions.Count;
			DebugTrace.Test("deep zoom level count: " + MaxDeepZoomLevel);
		}

		private void GetLevel0DeepZoomDownsamples()
		{
			Level0DeepZoomDownsamples = Enumerable.Range(0, MaxDeepZoomLevel)
				.Select(x => (int)Math.Pow(2, MaxDeepZoomLevel - x - 1)).ToArray();
		}

		private void GetBestSlideLevelsForDeepZoomLevels()
		{
			var bestLevelsForDeepZoomLevel = new List<int>();
			foreach (var d in Level0DeepZoomDownsamples)
			{
				var bestLevel = Osr.GetBestLevelForDownsample(d);
				bestLevelsForDeepZoomLevel.Add(bestLevel);
			}
			BestSlideLevelForDeepZoomLevel = bestLevelsForDeepZoomLevel.ToArray();
		}

		private void GetDeepZoomDownsamples()
		{
			DeepZoomLevelDownsamples = Enumerable.Range(0, MaxDeepZoomLevel).Select(
				level => Level0DeepZoomDownsamples[level] /
						 Osr.LevelDownsamples[BestSlideLevelForDeepZoomLevel[level]]
			).ToArray();
		}

		private void GetBackgroundColor()
		{
			var bgColorString = Osr.Properties[Osr.PROPERTY_NAME_BACKGROUND_COLOR];

			BackgroundColor = ColorTranslator.FromHtml("#" + bgColorString);
		}

		private void GetTilesCount()
		{
			var sum = TileDimensions.Sum(tileDim => tileDim.Width * tileDim.Height);
			DeepZoomTilesCount = (int)sum;
			DebugTrace.Test("total dz tiles count: " + DeepZoomTilesCount);
		}

		public Bitmap GetTile(int level, LevelSize adress)
		{
			// Read tile
			CheckParameters(level, adress);
			var col = adress.Width;
			var row = adress.Height;
			DebugTrace.Leave("start tile calculations " + level + "/" + col + "_" + row);

			// Get preferred slide level
			var preferedSlideLevel = BestSlideLevelForDeepZoomLevel[level];

			// Calculate top left and bottom right overlap
			var topLeftOverlap = new LevelSize(col == 0 ? 0 : DeepZoomOverlap,
				row == 0 ? 0 : DeepZoomOverlap);
			DebugTrace.Test("top left overlap: " + topLeftOverlap);

			var bottomRightOverlap = new LevelSize(col == TileDimensions[level].Width - 1 ? 0 : DeepZoomOverlap,
				row == TileDimensions[level].Height - 1 ? 0 : DeepZoomOverlap);
			DebugTrace.Test("bottom right overlap: " + bottomRightOverlap);

			// Get final size of the tile
			var finalTileWidth = Math.Min(TileSize, DeepZoomLevelDimensions[level].Width - TileSize * col) +
								 topLeftOverlap.Width + bottomRightOverlap.Width;
			var finalTileHeight = Math.Min(TileSize, DeepZoomLevelDimensions[level].Height - TileSize * row) +
								  topLeftOverlap.Height + bottomRightOverlap.Height;
			var finalTileSize = new LevelSize(finalTileWidth, finalTileHeight);
			DebugTrace.Test("final Deep Zoom Tile Size: " + finalTileSize);

			if (finalTileSize.Width < 0 || finalTileSize.Height < 0)
				throw new ArgumentException($"out of bounds level {level}, row {row}, col {col}");

			// Obtain the region coordinates
			var deepZoomLocation = new LevelSize(TileSize * col, TileSize * row);
			DebugTrace.Test("deep zoom location coordinates: " + deepZoomLocation);
			var levelLocation = new SizeF(
				(float)DeepZoomLevelDownsamples[level] * (deepZoomLocation.Width - topLeftOverlap.Width),
				(float)DeepZoomLevelDownsamples[level] * (deepZoomLocation.Height - topLeftOverlap.Height));
			DebugTrace.Test("deep zoom level coordinates: " + levelLocation);

			// Round location down and size up, and add offset of active area
			var level0Location = new LevelSize(
				(long)(Osr.LevelDownsamples[preferedSlideLevel] * levelLocation.Width + Level0Offset.Width),
				(long)(Osr.LevelDownsamples[preferedSlideLevel] * levelLocation.Height + Level0Offset.Height));
			DebugTrace.Test("level 0 location coordinates: " + level0Location);

			var regionWidth = (long)Math.Min(Math.Ceiling(DeepZoomLevelDownsamples[level] * finalTileSize.Width),
				LevelDimensions[preferedSlideLevel].Width - Math.Ceiling(levelLocation.Width));
			var regionHeight = (long)Math.Min(Math.Ceiling(DeepZoomLevelDownsamples[level] * finalTileSize.Height),
				LevelDimensions[preferedSlideLevel].Height - Math.Ceiling(levelLocation.Height));
			var regionSize = new LevelSize(regionWidth, regionHeight);
			DebugTrace.Test("region size: " + regionSize);

			var tileBmp = Osr.ReadRegion(level0Location, preferedSlideLevel, regionSize);

			//Apply on background color (composite)
			tileBmp = ApplyOnBackgroundColor(tileBmp);

			// Scale to the correct size
			var deepZoomSize = finalTileSize;
			if (regionSize.Width == deepZoomSize.Width &&
				regionSize.Height == deepZoomSize.Height)
				return tileBmp;

			DebugTrace.Test("resize " + level + "/" + col + "_" + row + " to: " + deepZoomSize);
			tileBmp = new Bitmap(tileBmp, (int)deepZoomSize.Width, (int)deepZoomSize.Height);
			return tileBmp;
		}

		private Bitmap ApplyOnBackgroundColor(Bitmap bmp)
		{
			using (var src = new Bitmap(bmp))
			{
				bmp.Dispose();
				bmp = new Bitmap(src.Width, src.Height);
				using (var g1 = Graphics.FromImage(bmp))
				{
					g1.Clear(BackgroundColor);
					g1.DrawImage(src, 0, 0);
				}
			}
			return bmp;
		}
		
		private void CheckParameters(int level, LevelSize adress)
		{
			var col = adress.Width;
			var row = adress.Height;

			if (level < 0 || level >= MaxDeepZoomLevel)
				throw new ArgumentException($"wrong level level {level}, row {row}, col {col}");

			if (TileDimensions[level].Width <= col || TileDimensions[level].Height <= row ||
				0 > col || 0 > row)
				throw new ArgumentException($"wrong address level {level}, row {row}, col {col}");
		}
		
		public MemoryStream GetDziMetadata(string format)
		{
			/*"""Return a stream containing the XML metadata for the .dzi file.
			format:    the format of the individual tiles ('png' or 'jpeg')"""*/
			var updatedMetadataString = GetDziMetadataString(format);
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(updatedMetadataString);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}

		public string GetDziMetadataString(string format)
		{
			/*"""Return a string containing the XML metadata for the .dzi file.
			format:    the format of the individual tiles ('png' or 'jpeg')"""*/
			var width = Level0Dimensions.Width;
			var height = Level0Dimensions.Height;

			var updatedMetadataTemplate = DeepZoomImageTemplate.Replace("@Width", width.ToString())
				.Replace("@Height", height.ToString());
			updatedMetadataTemplate = updatedMetadataTemplate.Replace("@TileSize", TileSize.ToString())
				.Replace("@Overlap", DeepZoomOverlap.ToString());
			updatedMetadataTemplate = updatedMetadataTemplate.Replace("@Format", format);
			return updatedMetadataTemplate;
		}
	}
}