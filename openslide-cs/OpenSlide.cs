﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using OpenSlide.Cs.Helpers;

namespace OpenSlide.Cs
{
	public unsafe class OpenSlide : IDisposable
	{
		private readonly object slidesLock = new object();

		private int hashCodeVal;
		public string LIBRARY_VERSION = Marshal.PtrToStringAnsi(OpenSlideDll.openslide_get_version());

		private int* osr;
		public string PROPERTY_NAME_BACKGROUND_COLOR = "openslide.background-color";
		public string PROPERTY_NAME_BOUNDS_HEIGHT = "openslide.bounds-height";
		public string PROPERTY_NAME_BOUNDS_WIDTH = "openslide.bounds-width";
		public string PROPERTY_NAME_BOUNDS_X = "openslide.bounds-x";
		public string PROPERTY_NAME_BOUNDS_Y = "openslide.bounds-y";
		public string PROPERTY_NAME_COMMENT = "openslide.comment";
		public string PROPERTY_NAME_MPP_X = "openslide.mpp-x";
		public string PROPERTY_NAME_MPP_Y = "openslide.mpp-y";
		public string PROPERTY_NAME_OBJECTIVE_POWER = "openslide.objective-power";
		public string PROPERTY_NAME_QUICKHASH1 = "openslide.quickhash-1";
		public string PROPERTY_NAME_VENDOR = "openslide.vendor";

		public int MaxOpenSlideLevel { get; set; }
		public string OriginalFileName { get; set; }
		public Dictionary<string, string> Properties { get; set; }
		public Dictionary<string, AssociatedImage> AssociatedImages { get; set; }

		public int HashCodeVal
		{
			get => hashCodeVal;
			set => hashCodeVal = value;
		}

		public List<double> LevelDownsamples { get; set; }
		public List<LevelSize> LevelDimensions { get; set; }

		public OpenSlide(string fileName)
		{
			CheckIfFileExists(fileName);
			OriginalFileName = Path.GetFileName(fileName);

			ReadOpenSlide(fileName);
;
			GetMaxOpenSlideLevel();
			GetLevelDimensionsAndDownsamples();

			GetProperties();
			GetAssociatedImages();
			GetHashCodeVal();
		}

		public static string DetectVendor(string fileName)
		{
			return Marshal.PtrToStringAnsi(OpenSlideDll.openslide_detect_vendor(fileName));
		}

		private static void CheckIfFileExists(string fileName)
		{
			if (File.Exists(fileName))
				return;
			throw new OpenSlideException($"File '{fileName}' can't be opened");
		}

		private void ReadOpenSlide(string fileName)
		{
			osr = OpenSlideDll.openslide_open(fileName);
			if (osr == null || osr[0] == 0)
				CheckVendorIsValid(fileName);
		}

		private static void CheckVendorIsValid(string fileName)
		{
			var vendor = OpenSlideDll.openslide_detect_vendor(fileName);
			if (vendor.ToInt32() != 0)
				throw new OpenSlideUnsupportedFormatException("Vendor " + Marshal.PtrToStringAnsi(vendor) + " unsupported?");
			throw new OpenSlideUnsupportedFormatException("File unrecognized");
		}

		public string CheckError()
		{
			var errorMessage = OpenSlideDll.openslide_get_error(osr);
			if (errorMessage.ToInt32() != 0)
				throw new OpenSlideException("openslide error: " + Marshal.PtrToStringAnsi(errorMessage));
			return null;
		}

		private void GetMaxOpenSlideLevel()
		{
			MaxOpenSlideLevel = OpenSlideDll.openslide_get_level_count(osr);
			DebugTrace.Test("number of levels: " + MaxOpenSlideLevel);
			if (MaxOpenSlideLevel == -1)
				CheckError();
		}

		private void GetLevelDimensionsAndDownsamples()
		{
			LevelDimensions = new List<LevelSize>();
			LevelDownsamples = new List<double>();

			for (var level = 0; level < MaxOpenSlideLevel; level++)
			{
				DebugTrace.Test("level: " + level);
				GetDimensionsAtLevel(level);

				GetDownsampleAtLevel(level);
			}
		}

		private void GetDimensionsAtLevel(int level)
		{
			OpenSlideDll.openslide_get_level_dimensions(osr, level, out long width, out long height);
			DebugTrace.Test("width: " + width);
			DebugTrace.Test("height: " + height);
			if (width == -1 || height == -1)
				CheckError();

			LevelDimensions.Add(new LevelSize(width, height));
		}

		private void GetDownsampleAtLevel(int level)
		{
			var downsample = OpenSlideDll.openslide_get_level_downsample(osr, level);
			if (downsample == -1.0)
				CheckError();
			LevelDownsamples.Add(downsample);
		}

		private void GetProperties()
		{
			var propertyNames = GetPropertyNames();
			Properties = new Dictionary<string, string>();
			foreach (var s in propertyNames)
				Properties.Add(s, GetPropertyValue(s));
			AddMissingStandardProperties();
		}

		private IEnumerable<string> GetPropertyNames()
		{
			var propertyNames = StringMarshaller.Marshal(OpenSlideDll.openslide_get_property_names(osr));
			return propertyNames;
		}

		public string GetPropertyValue(string propertyName)
		{
			return Marshal.PtrToStringAnsi(OpenSlideDll.openslide_get_property_value(osr, propertyName));
		}

		private void AddMissingStandardProperties()
		{
			if (!Properties.ContainsKey(PROPERTY_NAME_BOUNDS_WIDTH))
				Properties[PROPERTY_NAME_BOUNDS_WIDTH] = LevelDimensions[0].Width.ToString();
			if (!Properties.ContainsKey(PROPERTY_NAME_BOUNDS_HEIGHT))
				Properties[PROPERTY_NAME_BOUNDS_HEIGHT] = LevelDimensions[0].Height.ToString();
			if (!Properties.ContainsKey(PROPERTY_NAME_BOUNDS_X))
				Properties[PROPERTY_NAME_BOUNDS_X] = "0";
			if (!Properties.ContainsKey(PROPERTY_NAME_BOUNDS_Y))
				Properties[PROPERTY_NAME_BOUNDS_Y] = "0";
			if (!Properties.ContainsKey(PROPERTY_NAME_MPP_X))
				Properties[PROPERTY_NAME_MPP_X] = "0";
			if (!Properties.ContainsKey(PROPERTY_NAME_MPP_Y))
				Properties[PROPERTY_NAME_MPP_Y] = "0";
			if (!Properties.ContainsKey(PROPERTY_NAME_BACKGROUND_COLOR))
				Properties[PROPERTY_NAME_BACKGROUND_COLOR] = "ffffff";
		}

		private void GetAssociatedImages()
		{
			var associatedImages = GetAssociatedImageNames();
			AssociatedImages = new Dictionary<string, AssociatedImage>();
			foreach (var s in associatedImages)
				AssociatedImages.Add(s, new AssociatedImage(s, this));
		}

		private IEnumerable<string> GetAssociatedImageNames()
		{
			var imageNames = StringMarshaller.Marshal(OpenSlideDll.openslide_get_associated_image_names(osr));
			return imageNames;
		}

		public Bitmap ReadAssociatedImage(string name)
		{
			lock (slidesLock)
			{
				CheckDisposed();

				OpenSlideDll.openslide_get_associated_image_dimensions(osr, name, out long width, out long height);

				CheckError();
				if (width == -1)
					throw new IOException("Failure reading associated image");

				var bmp = new Bitmap((int)width, (int)height);
				var bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite,
					PixelFormat.Format32bppArgb);

				var p = bmpdata.Scan0.ToPointer();
				OpenSlideDll.openslide_read_associated_image(osr, name, p);

				CheckError();
				return bmp;
			}
		}

		private void GetHashCodeVal()
		{
			var quickhash1 = Properties[PROPERTY_NAME_QUICKHASH1];
			if (quickhash1 != null)
				int.TryParse(quickhash1.Substring(0, 8), out hashCodeVal);
		}

		public void PrintAllProperties()
		{
			foreach (var property in Properties)
				Console.WriteLine(property.Key + ": " + property.Value);
		}

		public double GetLevelDownsample(int level)
		{
			return LevelDownsamples[level];
		}

		public int GetBestLevelForDownsample(double downsample)
		{
			// too small, return first
			if (downsample < LevelDownsamples[0])
				return 0;

			// find where we are in the middle
			for (var i = 1; i < MaxOpenSlideLevel; i++)
				if (downsample < LevelDownsamples[i])
					return i - 1;

			// too big, return last
			return MaxOpenSlideLevel - 1;
		}

		public Bitmap ReadRegion(LevelSize location, int level, LevelSize levelSize)
		{
			var bmp = new Bitmap((int)levelSize.Width, (int)levelSize.Height);
			bmp.SetPixel(0, 0, Color.AliceBlue);
			var bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite,
				PixelFormat.Format32bppArgb);

			var p = bmpdata.Scan0.ToPointer();
			OpenSlideDll.openslide_read_region(osr, p, location.Width, location.Height, level, levelSize.Width, levelSize.Height);
			if (p == null)
				throw new OpenSlideException($"error reading region loc:{location}, level:{level}, size:{levelSize}");

			bmp.UnlockBits(bmpdata);

			if (bmp.GetPixel(0, 0) == Color.Black)
			{
				var error = CheckError();
				if (error != null)
					throw new OpenSlideException($"error reading region loc:{location}, level:{level}, size:{levelSize}" + error);
			}
			DebugTrace.Leave("end ReadRegion " + level + "/" + location.Height + "_" + location.Width);
			return bmp;
		}

		public Bitmap GetThumbnail(int size)
		{
			/*"""Return a PIL.Image containing an RGB thumbnail of the image.
			size: the maximum size of the thumbnail."""*/
			var downsample = Math.Max(LevelDimensions[0].Width / size, LevelDimensions[0].Height / size);
			var level = GetBestLevelForDownsample(downsample);
			var tile = ReadRegion(new LevelSize(0, 0), level, LevelDimensions[level]);

			return tile;
		}

		// call with the reader lock held
		private void CheckDisposed()
		{
			if (osr == null)
				throw new OpenSlideDisposedException();
		}

		public string GetLibraryVersion()
		{
			return LIBRARY_VERSION;
		}

		public override int GetHashCode()
		{
			return HashCodeVal;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;

			var os2 = (OpenSlide)obj;
			var quickhash1 = Properties[PROPERTY_NAME_QUICKHASH1];
			var os2Quickhash1 = os2.Properties[PROPERTY_NAME_QUICKHASH1];

			if (quickhash1 != null && os2Quickhash1 != null)
				return quickhash1.Equals(os2Quickhash1);
			if (quickhash1 == null && os2Quickhash1 == null)
				return OriginalFileName.Equals(os2.OriginalFileName);
			return false;
		}

		#region IDisposable Support

		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (disposedValue) return;
			if (disposing)
			{
			}
			if (osr != null && osr[0] != 0)
			{
				OpenSlideDll.openslide_close(osr);
			}

			disposedValue = true;
		}

		~OpenSlide()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		

				public void Close()
				{
					Dispose();
				}
/*
				public void Dispose()
				{
					lock (slides_lock)
					{
						if (osr != null && osr[0] != 0)
						{
							OpenSlideDll.openslide_close(osr);
							osr = null;
						}
					}

					GC.SuppressFinalize(this);
				}
				*/
				#endregion
	}
}