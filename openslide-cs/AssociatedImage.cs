﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using OpenSlide.Cs.Helpers;

namespace OpenSlide.Cs
{
	public class AssociatedImage : object
	{
		public AssociatedImage(string name, OpenSlide osr)
		{
			if (name == null || osr == null)
				throw new NullReferenceException("Arguments cannot be null");
			Name = name;
			Osr = osr;
		}

		public OpenSlide Osr { get; }

		public string Name { get; }

		public Bitmap ToBitmap()
		{
			return Osr.ReadAssociatedImage(Name);
		}

		public void WriteToFile(string writePath)
		{
			var exactPathToWrite = CreatePathToWriteBitmap(writePath);

			try
			{
				Directory.CreateDirectory(writePath);
				using (var bitmap = ToBitmap())
				{
					if (bitmap == null) return;
					bitmap.Save(exactPathToWrite);
					DebugTrace.Leave("File " + Name + " succesfully saved.");
				}

			}
			catch (Exception)
			{
				DebugTrace.Leave("There was a problem saving the file." + "\nCheck the file permissions.");
			}
		}

		private string CreatePathToWriteBitmap(string writePath)
		{
			var exactWritePath = new StringBuilder();
			var slideNameWithoutExtension = Path.GetFileNameWithoutExtension(Osr.OriginalFileName);

			exactWritePath.Append(writePath).Append("/").Append(slideNameWithoutExtension).Append("_").Append(Name)
				.Append(".bmp");
			DebugTrace.Leave(exactWritePath.ToString());
			return exactWritePath.ToString();
		}

		public override int GetHashCode()
		{
			return Osr.GetHashCode() + Name.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;

			var ai2 = (AssociatedImage)obj;
			return Osr.Equals(ai2.Osr) && Name.Equals(ai2.Name);
		}
	}
}