﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Drawing;

namespace OpenSlide.Cs.Test
{ 
	[TestClass]
	public class BoxesDeepZoomTest
	{
		private OpenSlide osr;
		private DeepZoomGenerator dz;
		private static string TestFileName = "tests/boxes.tiff";

		[TestInitialize]
		public void SetUp()
		{
			osr = new OpenSlide(TestFileName);
			dz = new DeepZoomGenerator(osr, 254, 1);
		}

		[TestCleanup]
		public  void TearDown()
		{
			osr.Dispose();
		}

		[TestMethod]
		public void TestMetadata()
		{
			Assert.AreEqual(dz.MaxDeepZoomLevel, 10);
			Assert.AreEqual(dz.DeepZoomTilesCount, 11);
			LevelSize[] testTileDimensions = {  new LevelSize(1, 1), new LevelSize(1, 1), new LevelSize(1, 1),
												new LevelSize(1, 1), new LevelSize(1, 1), new LevelSize(1,1),
												new LevelSize(1,1), new LevelSize(1,1), new LevelSize(1,1),
												new LevelSize(2,1) };
			CollectionAssert.AreEqual(dz.TileDimensions, testTileDimensions);

			var testLevelDimensions = new List<LevelSize>
			{
			new LevelSize(1,1), new LevelSize(2,1), new LevelSize(3,2), new LevelSize(5,4),
			new LevelSize(10,8), new LevelSize(19,16), new LevelSize(38,32), new LevelSize(75,63),
			new LevelSize(150,125), new LevelSize(300,250)
			};
			var dzLevelDimensions = dz.DeepZoomLevelDimensions;
			CollectionAssert.AreEqual(dzLevelDimensions, testLevelDimensions);
		}

		[TestMethod]
		public void TestGetTile()
		{
			Assert.AreEqual(dz.GetTile(9, new LevelSize(1, 0)).Size, new Size(47, 250));
		}

		[TestMethod]
		public void TestGetTileBadLevel()
		{
			MyAssert.Throws<ArgumentException>(() => dz.GetTile(-1, new LevelSize(0, 0)));
			MyAssert.Throws<ArgumentException>(() => dz.GetTile(10, new LevelSize(0, 0)));
		}

		[TestMethod]
		public void TestGetTileBadAdress()
		{
			MyAssert.Throws<ArgumentException>(() => dz.GetTile(0, new LevelSize(-1, 0)));
			MyAssert.Throws<ArgumentException>(() => dz.GetTile(0, new LevelSize(1, 0)));
		}

		[TestMethod]
		public void TestGetTileDimensions()
		{
			Assert.AreEqual(dz.GetTileDimensions(9, new LevelSize(1, 0)), new LevelSize(47, 250));
		}
		
		[TestMethod]
		public void TestGetTileCoordinates()
		{
			TileCoordinates dz_tc = dz.GetTileCoordinates(9, new LevelSize(1, 0));
			TileCoordinates tc = new TileCoordinates(new LevelSize(253, 0), 0, new LevelSize(47, 250));
			Assert.AreEqual(dz_tc, tc);
		}

		[TestMethod]
		public void TestGetDZIMetadataString()
		{
			Assert.IsTrue(dz.GetDziMetadataString("jpeg").Contains("http://schemas.microsoft.com/deepzoom/2008"));
			Assert.IsTrue(dz.GetDziMetadataString("jpeg").Contains("jpeg"));
			Assert.IsTrue(dz.GetDziMetadataString("jpeg").Contains("254"));
			Assert.IsTrue(dz.GetDziMetadataString("jpeg").Contains("1"));
		}
	}
}
