﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OpenSlide.Cs.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using OpenSlide.Cs;

namespace OpenSlide.Cs.Test
{
	class TestOpenSlide
	{
		private static string testSVS = "JP2K-33003-1.svs";
		private static string testTIFF = "CMU-1.tiff";
		private static string testMIRAX = "Mirax2.2-4-PNG.mrxs";
		private static string testNDPI = "CMU-2.ndpi";
		private static string testSCN = "Leica-1.scn";
		private static string testTIF = "CMU-3.tif";
		private static string testVMS = "CMU-1-40x - 2010-01-12 13.24.05.vms";
		private static string testBIF = "OS-2.bif";
		private static string testExtension;
		static OpenSlide slide;

		public static void Main()
		{
			//TestReadingFormatsAndInfo();
			TestReadingBIF();

			Console.WriteLine("Press any key to quit...");
			Console.ReadKey();
		}

		private static void TestReadingFormatsAndInfo()
		{
			TestReadingMIRAX();
			TestReadingInfo();
			TestClose();

			TestReadingNDPI();
			TestReadingInfo();
			TestClose();

			TestReadingSCN();
			TestReadingInfo();
			TestClose();

			TestReadingSVS();
			TestReadingInfo();
			TestClose();

			TestReadingTIF();
			TestReadingInfo();
			TestClose();

			TestReadingTIFF();
			TestReadingInfo();
			TestClose();

			TestReadingVMS();
			TestReadingInfo();
			TestClose();

			TestReadingBIF();
			TestReadingInfo();
			TestClose();
		}

		public static void TestReadingInfo()
		{
			TestDownsamples();
			TestBiggestDownsamples();
			//TestSaveAssociatedImages();
			//DebugTrace.Leave("\n");
			TestPrintProperties();
			TestLibraryVersion();
		}

		public static void TestLibraryVersion()
		{
			try
			{
				Console.WriteLine(slide.GetLibraryVersion());
			}
			catch (Exception)
			{
				DebugTrace.Leave("Couldn't get to the library!");
			}
		}

		private static void TestReadingVariousFormats()
		{
			TestReadingMIRAX();
			TestClose();

			TestReadingNDPI();
			TestClose();

			TestReadingSCN();
			TestClose();

			TestReadingSVS();
			TestClose();

			TestReadingTIF();
			TestClose();

			TestReadingTIFF();
			TestClose();

			TestReadingVMS();
			TestClose();

			TestReadingBIF();
			TestClose();
		}

		public static void TestReadingSVS()
		{
			testExtension = "SVS";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testSVS);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingTIFF()
		{
			testExtension = "TIFF";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testTIFF);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingMIRAX()
		{
			testExtension = "MIRAX";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testMIRAX);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingNDPI()
		{
			testExtension = "NDPI";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testNDPI);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingSCN()
		{
			testExtension = "SCN";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testSCN);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingTIF()
		{
			testExtension = "TIF";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testTIF);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingVMS()
		{
			testExtension = "VMS";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testVMS);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestReadingBIF()
		{
			testExtension = "BIF";
			DebugTrace.Leave("* test reading format: " + testExtension);
			try
			{
				slide = new OpenSlide(testBIF);
			}
			catch (Exception e)
			{
				throw new Exception("Reading " + testExtension + " went wront", e);
			}
			DebugTrace.Leave("* test finished reading format: " + testExtension);
		}

		public static void TestClose()
		{
			DebugTrace.Leave("* closing " + testExtension + "file");
			if (slide != null)
				slide.Dispose();
			DebugTrace.Leave("* end closing " + testExtension + "file");
		}

		public static void TestDownsamples()
		{
			for (int level = 0; level < slide.MaxOpenSlideLevel; level++)
			{
				DebugTrace.Leave("level: " + level + " downsample: " + slide.GetLevelDownsample(level));
			}
		}

		public static void TestBiggestDownsamples()
		{
			TestNextBiggestDownsample(0.8);
			TestNextBiggestDownsample(1.0);
			TestNextBiggestDownsample(1.5);
			TestNextBiggestDownsample(2.0);
			TestNextBiggestDownsample(3.0);
			TestNextBiggestDownsample(3.1);
			TestNextBiggestDownsample(10);
			TestNextBiggestDownsample(20);
			TestNextBiggestDownsample(25);
			TestNextBiggestDownsample(100);
			TestNextBiggestDownsample(1000);
			TestNextBiggestDownsample(10000);
		}

		public static void TestNextBiggestDownsample(double downsample)
		{
			int level = slide.GetBestLevelForDownsample(downsample);
			DebugTrace.Leave("level for downsample " + downsample + ": " + level + " " + slide.GetLevelDownsample(level));
		}
		/*
		public static void TestSaveAssociatedImages()
		{
			try
			{
				slide.SaveAllAssociatedImages();
			}
			catch(Exception)
			{
				DebugTrace.Leave("Writing associated images for " + slide.OriginalFileName + " went wrong");
			}
		}
		*/
		public static void TestPrintProperties()
		{
			slide.PrintAllProperties();
		}
	}

	[TestClass]
	public class TestSlideWithoutOpening
	{

		[TestMethod()]
		public void TestDetectFormat()
		{
			Assert.IsTrue(OpenSlide.DetectVendor("nonExisting") == null);
			Assert.IsTrue(OpenSlide.DetectVendor("setup.py") == null);
			Assert.IsTrue(OpenSlide.DetectVendor("tests/boxes.tiff") == "generic-tiff");
		}

		[TestMethod()]
		public void TestOpen()
		{
			MyAssert.Throws<OpenSlideException>(() => new OpenSlide("does_not_exist"));
			MyAssert.Throws<OpenSlideException>(() => new OpenSlide("setup.py"));
			MyAssert.Throws<OpenSlideException>(() => new OpenSlide("tests/unopenable.tiff"));
		}

		[TestMethod()]
		[ExpectedException(typeof(AccessViolationException))]
		public void TestReadRegionOnClosedHandle()
		{
			var osr = new OpenSlide("tests/boxes.tiff");
			osr.Close();

			MyAssert.Throws<AccessViolationException>(() => osr.ReadRegion(new LevelSize(0, 0), 0, new LevelSize(100, 100)));
		}

		[TestMethod()]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void TestReadingAssociatedImagesOnClosedHandle()
		{
			var osr = new OpenSlide("tests/boxes.tiff");
			var associated = osr.AssociatedImages;

			osr.Close();

			var image = associated["label"];
		}

		[TestMethod()]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void TestReadingPropertiesImagesOnClosedHandle()
		{
			var osr = new OpenSlide("tests/boxes.tiff");
			var props = osr.Properties;
			osr.Close();

			var prop = props["PROPERTY_NAME_VENDOR"];
		}

		[TestMethod()]
		public void TestContextManager()
		{
			var osr = new OpenSlide("tests/boxes.tiff");
			Assert.AreEqual(osr.MaxOpenSlideLevel, 4);
			//MyAssert.Throws<Exception>(osr.MaxOpenSlideLevel);
		}
	}

	[TestClass]
	public class TestSlide
	{
		private OpenSlide osr;
		string filename = "tests/boxes.tiff";

		[TestInitialize]
		public void SetUp()
		{
			var currentDir = Directory.GetCurrentDirectory();
			osr = new OpenSlide(filename);
		}

		[TestCleanup]
		public void TearDown()
		{
			osr.Close();
		}

		[TestMethod]
		public void TestBasicMetadata()
		{
			Assert.AreEqual(osr.MaxOpenSlideLevel, 4);

			List<LevelSize> testLevelDimensions = new List<LevelSize>(4)
			{
				new LevelSize(300, 250),
				new LevelSize(150, 125),
				new LevelSize(75, 62),
				new LevelSize(37, 31)
			};

			CollectionAssert.AreEqual(osr.LevelDimensions, testLevelDimensions);
			Assert.AreEqual(osr.LevelDimensions[0], new LevelSize(300, 250));
			Assert.AreEqual(osr.LevelDownsamples.Count, osr.MaxOpenSlideLevel);
			Assert.AreEqual((int)osr.LevelDownsamples[2], 4);
			Assert.AreEqual((int)osr.LevelDownsamples[3], 8);

			Assert.AreEqual(osr.GetBestLevelForDownsample(0.5), 0);
			Assert.AreEqual(osr.GetBestLevelForDownsample(3), 1);
			Assert.AreEqual(osr.GetBestLevelForDownsample(37), 3);
		}


		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void TestProperties()
		{
			Assert.AreEqual(osr.Properties["openslide.vendor"], "generic-tiff");
			var nonexisting = osr.Properties["nonExisting"];

			//Assert.AreEqual()
		}

		[TestMethod]
		public void TestReadRegion()
		{
			Assert.AreEqual(osr.ReadRegion(new LevelSize(-10, -10), 1, new LevelSize(400, 400)).Size, new Size(400, 400));
		}


		[TestMethod]
		public void TestReadRegionBadLevel()
		{
			Assert.AreEqual(osr.ReadRegion(new LevelSize(0, 0), 4, new LevelSize(100, 100)).Size, new Size(100, 100));
		}

		[TestMethod]
		public void TestReadRegionBadSize()
		{
			MyAssert.Throws<Exception>(() => osr.ReadRegion(new LevelSize(0, 0), 1, new LevelSize(400, -5)));
		}

	}

	[TestClass]
	public class TestAperioSlide
	{
		private OpenSlide osr;
		string filename = "tests/small.svs";

		[TestInitialize]
		public void SetUp()
		{
			var currentDir = Directory.GetCurrentDirectory();
			osr = new OpenSlide(filename);
		}

		[TestCleanup]
		public void TearDown()
		{
			osr.Close();
		}

		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void TestAssociatedImages()
		{
			Assert.AreEqual(osr.ReadAssociatedImage("thumbnail").Size, new Size(16, 16));
			var nonexisting = osr.AssociatedImages["nonExisting"];
		}
	}

	[TestClass]
	public class TestUnreadableSlide
	{
		private OpenSlide osr;
		string filename = "tests/unreadable.svs";

		[TestInitialize]
		public void SetUp()
		{
			osr = new OpenSlide(filename);
		}

		[TestCleanup]
		public void TearDown()
		{
			osr.Close();
		}

		[TestMethod]
		public void TestReadBadRegion()
		{
			Assert.AreEqual(osr.Properties["openslide.vendor"], "aperio");
			//MyAssert.Throws<Exception>(() => osr.ReadRegion(new LevelSize(0, 0), 0, new LevelSize(16, 16)));
		}

		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void TestReadBadAssociatedImage()
		{
			Assert.AreEqual(osr.Properties["openslide.vendor"], "aperio");
			var nonexisting = osr.AssociatedImages["macro"];
		}
	}
}